﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace QuestionSet
{
    public class Question
    {
        public static List<Question> QuestionPaper = new List<Question>();
        private static object stp;

        public char Set { get; set; }
        public int Code { get; set; }

        public Question(char Set, int Code)
        {
            this.Set = Set;
            this.Code = Code;
        }
        public Question()
        {

        }

        public void PaperCreate()
        {
            int temp, i = 0, temp1 = 0;

            Stopwatch stp = new Stopwatch();
            Random rand = new Random();
            stp.Start();
            while (i < 50000)
            {

                temp = rand.Next(1, 5);
                if (temp1 == temp)
                    continue;
                else
                {
                    temp1 = temp;
                    if (temp == 1)
                    {
                        QuestionPaper.Add(new Question('A', 1));
                    }
                    if (temp == 2)
                    {
                        QuestionPaper.Add(new Question('B', 2));
                    }
                    if (temp == 3)
                    {
                        QuestionPaper.Add(new Question('C',3));
                    }
                    if (temp == 4)
                    {
                        QuestionPaper.Add(new Question('D', 4));
                    }
                    i++;
                }
            }
            stp.Stop();
            Console.WriteLine(stp.ElapsedMilliseconds);
            display();
            Console.ReadLine();
        }
        public static void display()
        {
            Stopwatch stp = new Stopwatch();
            stp.Start();
            for (int i = 0; i < 50000; i++)
            {
                Console.Write(QuestionPaper[i].Code);
            }
            stp.Stop();
            Console.WriteLine("\n\n"+stp.ElapsedMilliseconds);
        }
    }
}

