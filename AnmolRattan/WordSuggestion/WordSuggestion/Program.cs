﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stpwatch = new Stopwatch();
            stpwatch.Start();
            Console.WriteLine("Enter input");
            string inp = Console.ReadLine();
            WordBank wb = new WordBank();
            
            Parallel.For(0, WordBank.wordbank.Length / 10000, x => wb.MatchWord(inp));
            stpwatch.Stop();
            Console.WriteLine("Total Program Time: " + stpwatch.ElapsedMilliseconds);
            Console.ReadLine();
        }
    }
}
